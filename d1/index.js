// get data
fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => response.json())
.then(data => showPosts(data));


// show data
const showPosts = (posts) =>
{
    let postEntries = "";

    posts.forEach(post => 
    {
        postEntries += `
            <div id = "post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onclick="editPost(${post.id})">Edit</button>
                <button onclick="deletePost(${post.id})">Delete</button>
            </div>
        `
    });

    document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// add post data
document.querySelector("#form-add-post").addEventListener("submit", e =>
{
    e.preventDefault();

    fetch("https://jsonplaceholder.typicode.com/posts", 
    {
        method: "POST",
        body: JSON.stringify({
            title: document.querySelector("#txt-title").value,
            body: document.querySelector("#txt-body").value,
            userId: 1
        }),
        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
    .then(response => response.json())
    .then(data => 
    {
        console.log(data);
        alert("Successfully added");

        document.querySelector("#txt-title").value = null;
        document.querySelector("#txt-body").value = null;
    })
})

// edit post
const editPost = (postId) =>
{
    let title = document.querySelector(`#post-title-${postId}`).innerHTML;
    let body = document.querySelector(`#post-body-${postId}`).innerHTML;

    document.querySelector("#txt-edit-id").value = postId;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;

    document.querySelector("#btn-submit-update").removeAttribute("disabled");
}

// update post on frontend only
/* document.querySelector("#form-edit-post").addEventListener("submit", (e) =>
{
    e.preventDefault();
    let postId = document.querySelector("#txt-edit-id").value;
    let title = document.querySelector("#txt-edit-title").value,
        body = document.querySelector("#txt-edit-body").value;

    
    document.querySelector(`#post-title-${postId}`).innerHTML = title;
    document.querySelector(`#post-body-${postId}`).innerHTML = body;

    alert("Post updated.");
    //showPosts(posts);  
}); */

// update post using fetch
document.querySelector("#form-edit-post").addEventListener("submit", (e) =>
{
    e.preventDefault();
    let postId = document.querySelector("#txt-edit-id").value;

    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, 
    {
        method: "PUT",
        body: JSON.stringify({
            id: postId,
            title: document.querySelector("#txt-edit-title").value,
            body: document.querySelector("#txt-edit-body").value,
            userId: 1
        }),
        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
    .then(response => response.json())
    .then(data => 
    {
        //console.log(data);
        alert("Post updated");

        document.querySelector(`#post-title-${postId}`).innerHTML = data.title;
        document.querySelector(`#post-body-${postId}`).innerHTML = data.body;

        document.querySelector("#txt-edit-id").value = null
        document.querySelector("#txt-edit-title").value = null;
        document.querySelector("#txt-edit-body").value = null;
        document.querySelector("#btn-submit-update").setAttribute("disabled", true);
    })
});


// DELETE POST - S49 ACTIVITY
const deletePost = (postId) =>
{
    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
        method: 'DELETE',
    }) 
    document.querySelector(`#post-${postId}`).remove();
}

